//
//  User.swift
//  testFireBase
//
//  Created by Igor Medelyan on 7/25/18.
//  Copyright © 2018 ekreative. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

struct User {
    
    let uid: String
    let email: String
    
    init(authData: Firebase.User) {
        uid = authData.uid
        email = authData.email!
    }
    
    init(uid: String, email: String) {
        self.uid = uid
        self.email = email
    }
}
