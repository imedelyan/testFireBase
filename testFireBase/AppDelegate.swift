//
//  AppDelegate.swift
//  testFireBase
//
//  Created by Igor Medelyan on 7/25/18.
//  Copyright © 2018 ekreative. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        Database.database().isPersistenceEnabled = true
        
        return true
    }
}

